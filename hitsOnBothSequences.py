# Author: Marco Munoz Perez
# Email: marcomunozperez@uma.es

# In[6]:

import matplotlib.pyplot as plt
from tqdm import *


# In[7]:

def get_line_number(file_path):
    """
    Returns the total number of lines in the file.
    Only used for progress verbose purposes
    :param file_path:
    :return:
    """
    with open(file_path, 'r') as f:
        i = 0
        for line in f:
            i += 1
        return i


def get_kmer_pair(file_reader):
    value1 = ''
    value2 = ''
    while file_reader.read(1) != ',':
        file_reader.seek(file_reader.tell() - 1, 0)
        value1 += file_reader.read(1)

    while file_reader.read(1) != ';':
        file_reader.seek(file_reader.tell() - 1, 0)
        value2 += file_reader.read(1)

    return [int(value1), int(value2)]


# In[8]:


def get_pairs_for_kmer(seq1, seq2, k, file_reader, hit_rate, miss_rate, plot=False):
    ret = list()
    while file_reader.read(1) != '\n':
        file_reader.seek(file_reader.tell() - 1, 0)
        ret.append(expand_single_hit(seq1, seq2, get_kmer_pair(file_reader), k, hit_rate, miss_rate, plot))
    return ret


# In[9]:


def expand_single_hit(seq1, seq2, hit, k, hit_rate=1, miss_rate=1, plot=False):
    hit = list(map(int, hit))
    if plot is True:
        positions = dict()
        x_axis = list()
        y_axis = list()
    count_fwd = 1
    max_fwd = 1
    max_pos_fwd = hit[0]
    count_bwd = 1
    max_bwd = 1
    max_pos_bwd = hit[0]

    pos1, pos2 = hit
    while count_bwd > 0 and pos1 > 0 and pos2 > 0:
        if plot is True:
            x_axis.append(pos1)

        if seq1[pos1:pos1 + k] == seq2[pos2:pos2 + k]:
            count_bwd += hit_rate
            if count_bwd > max_bwd:
                max_bwd = count_bwd
                max_pos_bwd = pos1
        else:
            # print(seq1[pos1:pos1 + k] + '!=' + seq2[pos2:pos2 + k])
            count_bwd -= miss_rate
        if plot is True:
            y_axis.append(count_bwd)

        pos1 -= 1
        pos2 -= 1
    if plot is True:
        x_axis.reverse()
        y_axis.reverse()
    pos1, pos2 = hit
    while count_fwd > 0 and pos1 < len(seq1) - k and pos2 < len(seq2) - k:
        if plot is True:
            x_axis.append(pos1)
        if seq1[pos1:pos1 + k] == seq2[pos2:pos2 + k]:
            count_fwd += hit_rate
            if count_fwd > max_fwd:
                max_fwd = count_fwd
                max_pos_fwd = pos1
        else:
            # print(seq1[pos1:pos1 + k] + '!=' + seq2[pos2:pos2 + k])
            count_fwd -= miss_rate
        if plot is True:
            y_axis.append(count_fwd)
        pos1 += 1
        pos2 += 1
    if plot is True:
        plt.plot(x_axis, y_axis)
        plt.show()
        plt.savefig('BiggestFrag.png')
    return [max_pos_fwd - max_pos_bwd] + hit


# In[10]:


def get_pairs_from_file(seq1, seq2, k, file_path_in, file_path_out, hit_rate=1, miss_rate=-1, plot=False,
                        progress=False):
    file_out = open(file_path_out, 'w')
    file_reader = open(file_path_in, 'r')
    file_reader.readline()  # Skip the first line. The firt line in hits file is blank

    if progress is True:
        file_reader = tqdm(file_reader, total=get_line_number(file_path_in))

    for line in file_reader:
        ret = list()
        line = line.split(' ')
        kmer = line[0]
        for pair in line[1].split(';')[:-1]:
            pair_list = pair.split(',')
            ret.append(expand_single_hit(
                seq1, seq2,
                [pair_list[0], pair_list[1].strip(';')],
                k, hit_rate, miss_rate, plot))

        for value in ret:
            file_out.write(str(value[0]) + ' ' + kmer + ' ' + str(value[1]) + ' ' + str(value[2]) + '\n')

    file_out.close()
    file_reader.close()


# In[]:

def get_first_frag(hits_filepath):
    file_reader = open(hits_filepath, 'r')
    line = file_reader.readline().split()
    frag_size = line[0]
    kmer = line[1]
    hit_position1 = line[2]
    hit_position2 = line[3]
    return [frag_size, kmer, hit_position1, hit_position2]
