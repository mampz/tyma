# Author: Marco Munoz Perez
# Email: marcomunozperez@uma.es
import subprocess
import os
from hitsOnBothSequences import *
import sequenceKmersPositions
from Bio import SeqIO
import sys


class FragmentAlign:
    def __init__(self, sequence_path1, sequence_path2, kmer_size, hits_file=None):
        """
        Stores both sequences as strings.
        Stores kmer size as k.
        Creates hits file if none is provided.
        :param sequence_path1:
        :param sequence_path2:
        :param hits_file:
        """
        # Read first sequence from both fasta input files
        self.sequence1 = list(SeqIO.parse(sequence_path1, "fasta"))[0]
        self.sequence2 = list(SeqIO.parse(sequence_path2, "fasta"))[0]
        # Store k size passed as parameter
        self.k = kmer_size
        if hits_file is None:
            # A file with kmers followed by respective positions in both sequences
            # is necessary in order to extend each one of these hits and find the
            # biggest fragment. If no hits file is provided, one will be created.
            self.hits_file = self.get_hits(self.get_kmers())
        else:
            # If a hits file is provided, this will be used.
            self.hits_file = hits_file

    def get_kmers(self):
        """
        In order to generate a hits file, these hits will be obtained from kmers
        from each of both sequences. In order to obtain these kmers, two files,
        one for each sequence, will be created. These will contain lines that
        consist of a k size kmer, indicated as a parameter (self.k) and its position.

        :return: List of file paths for the sorted file names.
        """
        print('Generating kmer files...(1/3)')
        if os.name == 'nt':
            # Sorting files is done with the terminal command sort for UNIX systems
            raise RuntimeError('Executing on Windows systems is not supported.') from os.error

        local_dir = 'KmerPositions/'  # Generated kmer files will be stored here
        file_exstension = '_k=' + str(self.k) + '_kmerPositions.txt'
        dest_filepath = [local_dir + 'sequence1' + file_exstension,
                         local_dir + 'sequence2' + file_exstension]
        # Once each of the files are generated, they will be sorted by kmer and the
        # sorted files will be saved to the following path:
        dest_filepath_sorted = [local_dir + 'sequence1' + 'Sorted' + file_exstension,
                                local_dir + 'sequence2' + 'Sorted' + file_exstension]
        # The following call will execute a function that will generate the kmer-position
        # file from the sequence passed as string and will save it to the destination folder.
        sequenceKmersPositions.str_to_kmer_positions(str(self.sequence1.seq),
                                                     self.k, dest_filepath[0])
        # The file will be sorted with the respective UNIX command
        # WARNING! THIS CALL WILL ONLY WORK ON UNIX-LIKE ENVIRONMENTS!
        subprocess.Popen(['sort', dest_filepath[0], '-o', dest_filepath_sorted[0]]).wait()
        # Same for the second file
        sequenceKmersPositions.str_to_kmer_positions(str(self.sequence2.seq),
                                                     self.k, dest_filepath[1])
        subprocess.Popen(['sort', dest_filepath[1], '-o', dest_filepath_sorted[1]]).wait()
        return dest_filepath_sorted

    @staticmethod
    def get_hits(kmers_filepath_list):
        """
        Obtains a hits file from two kmer files and stores them in KmerPositions/ folder.
        :param kmers_filepath_list: Paths to ordered kmer files
        :return: Hits output path string
        """
        print('Generating hits file...(2/3)')
        local_dir = 'KmerPositions/'  # Destination folder
        dest_filepath = local_dir + 'kmerHits.txt'
        # This calls the function that will generate the kmer hits file
        sequenceKmersPositions.get_kmer_hits_file(
            kmers_filepath_list[0],
            kmers_filepath_list[1], dest_filepath)
        return dest_filepath

    def get_fragemts(self, plot=True):
        print('Expanding fragments...(3/3)')
        local_filepath = 'frags/'
        frags_filepath = local_filepath + 'frags.txt'
        frags_filepath_sorted = local_filepath + 'Sorted' + 'frags.txt'
        hit_coef = 1  # Hit coefficient: added to the score for each hit
        miss_coef = 4  # Miss coefficient: subtracted to the score for each hit
        get_pairs_from_file(str(self.sequence1.seq),
                            str(self.sequence2.seq),
                            self.k,
                            self.hits_file,
                            frags_filepath,
                            hit_coef,
                            miss_coef, progress=True)
        subprocess.Popen(['sort', frags_filepath, '-o',
                          frags_filepath_sorted, '-n', '-r']).wait()
        biggest_hit = get_first_frag(frags_filepath_sorted)
        expand_single_hit(str(self.sequence1.seq),
                          str(self.sequence2.seq),
                          [biggest_hit[2],
                           biggest_hit[3]],
                          self.k,
                          hit_coef,
                          miss_coef,
                          plot)
        return biggest_hit


if __name__ == "__main__":
    file1 = sys.argv[1]
    file2 = sys.argv[2]
    k = int(sys.argv[3])

    # time_start = time.time()
    fa = FragmentAlign(file1, file2, k)
    biggest_frag = fa.get_fragemts()
    # time_end = time.time()
    print('Biggest fragment:')
    print('Fragment size: ' + biggest_frag[0])
    print('kmer seed: ' + biggest_frag[1])
    print('Fragment start position: ' + biggest_frag[2])
    print('Fragment end position: ' + biggest_frag[3])
    # print('Total execution time: ' + str(round(time_end - time_start, 2)) + ' s')