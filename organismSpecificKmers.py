from Bio import SeqIO
from tqdm import tqdm
import matplotlib.pyplot as plt


def get_freqs(sequence, k):
    freq_dict = dict()
    for i in range(len(sequence) - k):
        kmer = sequence[i:i + k]
        freq_dict[kmer] = freq_dict.get(kmer, 0) + 1
    return freq_dict


def get_count_of_kmer_1(sequence, k_range):
    low_kmer_dict = dict()
    for k in tqdm(list(k_range)):
        freq_dict = get_freqs(sequence, k)
        for kmer in freq_dict:
            if freq_dict[kmer] == 1:
                low_kmer_dict[k] = low_kmer_dict.get(k, 0) + 1
    return low_kmer_dict


def lowest_unique_kmers(sequence_list, k_range):
    for sequence in sequence_list:
        low_kmers = get_count_of_kmer_1(str(sequence.seq), k_range)
        print('> ' + sequence.id + ' k=' + str(min(low_kmers.keys())) + '-> ' + str
        (low_kmers[min(low_kmers.keys())]) + ' unique kmers')


def normalize_count(kmer_dict, seq_len):
    norm_dict = dict()
    for kmer in kmer_dict:
        norm_dict[kmer] = kmer_dict[kmer] / (seq_len - kmer)
    return norm_dict
