import math

def readFile( fileName ):
   "This function reads a file and returns it as a variable"
   ficheroFasta  = open(fileName, 'r')
   return ficheroFasta

def processContent( fileObject ):
    "This function separates headers and sequences in different lists"
    headerList=list()
    sequenceList=list()
    currSeq=""

    for line in fileObject:
        if line[0]=='>':
            if currSeq!="":
                sequenceList.append(currSeq.replace('\n',''))
            headerList.append(line.replace('\n',''))
        else:
            currSeq+=line
    sequenceList.append(currSeq.replace('\n',''))
    return {"headers":headerList,"sequences":sequenceList}

def headerValues(s):
    "Returns gi, ref and description from the header."
    sep1=s.find('gi|')
    sep2=s.find('ref|')
    sep3=s.find('|',sep2+4)
    gi=s[sep1+3:sep2-1]
    ref=s[sep2+4:sep3]
    descr=s[sep3+1:]
    return {"gi":gi,"ref":ref,"descr":descr}


def getInverseComplementary( sequence ):
    "This function returns the reverse complementary"
    #The letters are mapped in order to obtain the complementary sequence.
    map = {'A' : 'T',
           'C' : 'G',
           'G' : 'C',
           'T' : 'A',
           }
    #The sequence is inverted with [::-1] and the string is set to upper case in case it is in lower case.
    inverseSeq = sequence[::-1].upper()
    inverseComplement=""
    #For each letter (nucleotid) of the string, the complementary match is obtained with the map dictionary defined before and appended to a new string.
    for nucleotid in inverseSeq:
            inverseComplement+=map[nucleotid]
    return inverseComplement


def getKmerFreq( sequence,k ):
    "This function returns a dictionary with all k-mer possible combinations and their respetive frequency in the sequence."
    sequence=sequence.upper()
    def numToBase4(n):
        "Returns the corresponding base 4 number of a base 10 number."
        convertString = "0123456789"
        if n < 4:
            return convertString[n]
        else:
            return numToBase4(n//4) + convertString[n%4]

    def getFreq( sequence, kmer ):
        "Returns the hits count of the kmer on the sequence."
        freq=0
        i=0
        #Keeps comparing substrings of the sequence with the kmer string until the last kmer that fits in the sequence.
        while i<len(sequence)-len(kmer)+1:
            if sequence[i:i+len(kmer)]==kmer:
                freq+=1
            i+=1
        return freq

    #Stores the number of combinations
    numCombi=math.pow(4,k)

    map = {'0' : 'A',
           '1' : 'C',
           '2' : 'G',
           '3' : 'T',
           }

    i=0
    kmerFreq=dict() #An entry for each kmer combination (key) and its corresponding frequency (value).
    kmer=''
    while i<numCombi: #While not all possible combinations are reached
        for num in str(numToBase4(i)):#For each digit of the corresponding base 4 number
            kmer+=map[num] #Append the mapped value to a string
        while len(kmer)<k:
            kmer='A'+kmer #If digits are missing (those to the left of the integer), append the corresponding value ('0':'A').
        kmerFreq[kmer]=getFreq(sequence,kmer) #Get the frequency of the current kmer.
        i+=1
        kmer='' #Reset the kmer variable.
    return kmerFreq #Return the kmers and frequencies dictionary.
