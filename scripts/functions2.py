# Marco Muñoz Pérez - 77232669A
# Bioinfomática, Ingeniería de la Salud, Univeridad de Málaga

import pandas as pd
from scipy import stats

def getHeaders(filePath):
    """Returns a dictionary with line:headerInfo structure obtained from a specified file path."""
    import collections
    headerList = collections.OrderedDict()
    with open(filePath) as myFile:
        for num, line in enumerate(myFile, 1):
            if line[0] == '>':
                headerList[num] = line.replace('\n', '')
    return headerList


def getCharNumber(filePath, headerLine, offset):
    "Returns a file reader from a certain line on..."
    f = open(filePath, 'r')
    return f.seek(offset[headerLine])


def lineNumber(filePath):
    file = open(filePath, 'r')
    line_offset = []
    offset = 0
    for line in file:
        line_offset.append(offset)
        offset += len(line)
    return (line_offset)


def getKmerComb(k):
    "This function returns a dictionary with all k-mer possible combinations and their respetive frequency in the sequence."
    import math

    def numToBase4(n):
        "Returns the corresponding base 4 number of a base 10 number."
        convertString = "0123456789"
        if n < 4:
            return convertString[n]
        else:
            return numToBase4(n // 4) + convertString[n % 4]

    # Stores the number of combinations
    numCombi = math.pow(4, k)

    map = {'0': 'A',
           '1': 'C',
           '2': 'G',
           '3': 'T',
           }

    i = 0
    kmerFreq = dict()  # An entry for each kmer combination (key) and its corresponding frequency (value).
    kmer = ''
    while i < numCombi:  # While not all possible combinations are reached
        for num in str(numToBase4(i)):  # For each digit of the corresponding base 4 number
            kmer += map[num]  # Append the mapped value to a string
        while len(kmer) < k:
            kmer = 'A' + kmer  # If digits are missing (those to the left of the integer), append the corresponding value ('0':'A').
        kmerFreq[kmer] = 0  # Get the frequency of the current kmer.
        i += 1
        kmer = ''  # Reset the kmer variable.
    return kmerFreq  # Return the kmers and frequencies dictionary.


def getKmerFreq(filePath, i, k):
    file = open(filePath, 'r')
    lastPos = file.seek(0, 2)
    end = False
    kmerFreqs = getKmerComb(k)
    kmerSeq = ''
    c = 0
    while (end == False) and (file.seek(i, 0) <= file.seek(0, 2)):
        file.seek(i, 0)
        if round(i / lastPos, 2) != c:
            print('Progress: ' + str(int(round(i / lastPos, 2) * 100)) + '%\r', end='', flush=True)
            c = round(i / lastPos, 2)
        for n in range(k):
            char = file.read(1)
            while char == '\n':
                char = file.read(1)
            if char == '>':
                char = ''
                end = True
                i -= 1
            kmerSeq += char.upper()
        if len(kmerSeq) == k:
            if kmerSeq.find('N') < 0:
                kmerFreqs[kmerSeq] += 1
            # print(kmerSeq)
            # print(kmerSeq+': '+str(kmerFreqs[kmerSeq]))
        kmerSeq = ''
        i += 1
    file.close()
    return (kmerFreqs)


def createTable(headers, values):
    import plotly

    trace = go.Table(
        header=dict(values=headers,
                    line=dict(color='#7D7F80'),
                    fill=dict(color='#a1c3d1'),
                    align=['left'] * 5),
        cells=dict(values=values,
                   line=dict(color='#7D7F80'),
                   fill=dict(color='#EDFAFF'),
                   align=['left'] * 5))

    data = [trace]
    fig = dict(data=data)
    plotly.offline.plot(fig, filename='styled_table')


def dataFrame2Table(df):
    import plotly
    colVals = list()
    colVals.append(list(df.index))
    for column in df:
        colVals.append(list(df[column]))
    trace = plotly.graph_objs.Table(
        header=dict(values=df.columns.insert(0, 'Sequence'),
                    fill=dict(color='#C2D4FF'),
                    align=['left'] * 5),
        cells=dict(values=colVals,
                   fill=dict(color=['#25FEFD', '#F5F8FF']),
                   align=['left'] * 5))
    data = [trace]
    plotly.offline.plot(data, filename="pandas_table")


def getFreqAndTable(filePath, k):
    "Returns a table with the freqs for a given k"
    offset = lineNumber(filePath)
    headers = getHeaders(filePath)
    headerIndex = list()
    freqList = list()
    headerTableList = list()
    # headerTableList.append('k-mers'+'(k'+'='+str(k)+')')
    listValues = list()
    for header in headers:
        print(str(header) + ':' + headers[header])
        headerIndex.append(getCharNumber(filePath, header, offset))
        headerTableList.append(headers[header])
    for i, index in enumerate(headerIndex):
        print("Computing frecuencies for: " + headers[list(headers)[i]])
        freqList.append(getKmerFreq(filePath, index, k))
    # listValues.append(list(freqList[0].keys()))
    return ([headerTableList, freqList])


def createDataFrame(headerTableList, freqList):
    df = pd.DataFrame(freqList, index=headerTableList)
    return (df)


def addMean(dataFrame, type):
    """Calculates the mean for rows or columns"""
    res = dataFrame
    if type == 'column':
        res.loc['mean'] = res.mean(axis=0)
    elif type == 'row':
        res['mean'] = res.mean(axis=1)
    return res


def addStandardDev(dataFrame, type, mult):
    "Calculates the mean for rows or columns"
    res = pd.DataFrame()
    res = dataFrame
    tmp = pd.DataFrame(columns=res.columns)
    if type == 'column':
        tmp.loc['+stdev'] = res.std(axis=0)
        res.loc['+stdev'] = pd.concat([res['mean':'mean'], tmp.multiply(mult)]).sum()
        res.loc['-stdev'] = pd.concat([res['mean':'mean'], tmp.multiply(-mult)]).sum()
    elif type == 'row':
        res['stdev'] = res.std(axis=1)
    return res


def normalizeDF(dataFrame):
    res = pd.DataFrame()
    res = dataFrame
    res = res.div(res.sum(axis=1), axis=0)
    return res


def calculateRowZscore(dataFrame):
    res = pd.DataFrame()
    for column in dataFrame:
        res[column] = stats.zscore(dataFrame[column])
    res.index = dataFrame.index
    return res


def combineDF(dfList):
    return pd.concat(dfList)


def compareGenomes(pathList, k):
    import matplotlib.pyplot as plt
    import plotly
    traceList = list()
    dfList = list()
    for path in pathList:
        res = getFreqAndTable(path, k)
        df = createDataFrame(res[0], res[1])
        dfNorm = normalizeDF(df)
        dfList.append(dfNorm)
        h = getHeaders(path)
        traceList.append(
            plotly.graph_objs.Scatter(x=list(dfNorm), y=dfNorm.iloc[0], mode=h[list(h)[0]], name=h[list(h)[0]]))
    dfs = combineDF(dfList)
    dataFrame2Table(dfs)
    plotly.offline.plot(traceList, filename='basic-line')


def getFreqAndPickle(pathList, k):
    import os
    for path in pathList:
        res = getFreqAndTable(path, k)
        df = createDataFrame(res[0], res[1])
        fileName = os.path.basename(open(path, 'r').name)
        with open(fileName + '.pkl', 'wb') as handle:
            pickle.dump(df, handle, protocol=pickle.HIGHEST_PROTOCOL)


def readPkl(pklPath):
    import pickle
    with open(pklPath, 'rb') as handle:
        b = pickle.load(handle)
    return b


def compareGenomesFromPkls(pathList):
    import matplotlib.pyplot as plt
    import plotly
    traceList = list()
    dfList = list()
    for path in pathList:
        res = readPkl(path)
        df = createDataFrame(res[0], res[1])
        dfNorm = normalizeDF(df)
        dfList.append(dfNorm)
        h = res[0]
        traceList.append(plotly.graph_objs.Scatter(x=list(dfNorm), y=dfNorm.iloc[0], mode=h[0], name=h[0]))
    dfs = combineDF(dfList)
    dataFrame2Table(dfs)
    plotly.offline.plot(traceList, filename='basic-line')


def plotFromDf(dfNorm):
    import plotly
    traceList = list()
    for index, row in dfNorm.iterrows():
        if index.find('-stdev') != -1:
            traceList.append(
                plotly.graph_objs.Scatter(x=list(dfNorm), y=list(row), fill='tonexty', mode='lines', name=index,
                                          line=dict(color='rgb(184, 247, 212)')))
        elif index.find('+stdev') != -1:
            traceList.append(plotly.graph_objs.Scatter(x=list(dfNorm), y=list(row), mode='lines', name=index,
                                                       line=dict(color='rgb(184, 247, 212)')))
        else:
            traceList.append(plotly.graph_objs.Scatter(x=list(dfNorm), y=list(row), mode='lines+markers', name=index))
    layout = plotly.graph_objs.Layout(title='Gráfica de frecuencias', legend=dict(orientation="h"))
    # layout = plotly.graph_objs.Layout(title='Gráfica de frecuencias',showlegend=False)
    fig = plotly.graph_objs.Figure(data=traceList, layout=layout)
    plotly.offline.plot(fig, filename='basic-line')


def plotFromDfNoLegend(dfNorm, titleLabel):
    import plotly
    traceList = list()
    for index, row in dfNorm.iterrows():
        if index.find('-stdev') != -1:
            traceList.append(
                plotly.graph_objs.Scatter(x=list(dfNorm), y=list(row), fill='tonexty', mode='lines', name=index,
                                          line=dict(color='rgb(184, 247, 212)')))
        elif index.find('+stdev') != -1:
            traceList.append(plotly.graph_objs.Scatter(x=list(dfNorm), y=list(row), mode='lines', name=index,
                                                       line=dict(color='rgb(184, 247, 212)')))
        else:
            traceList.append(plotly.graph_objs.Scatter(x=list(dfNorm), y=list(row), mode='lines+markers', name=index))
    layout = plotly.graph_objs.Layout(title=titleLabel, showlegend=False)
    # layout = plotly.graph_objs.Layout(title='Gráfica de frecuencias',showlegend=False)
    fig = plotly.graph_objs.Figure(data=traceList, layout=layout)
    plotly.offline.plot(fig, filename='basic-line')


def plotBarchart(dfNorm):
    import plotly
    traceList = list()
    for index, row in dfNorm.iterrows():
        if index.find('+stdev') != -1:
            traceList.append(plotly.graph_objs.Bar(x=list(dfNorm), y=list(row), name=index))
        else:
            traceList.append(plotly.graph_objs.Bar(x=list(dfNorm), y=list(row), name=index))
    layout = plotly.graph_objs.Layout(title='Distancia de frecuencias entre organismos', barmode='overlay')
    fig = plotly.graph_objs.Figure(data=traceList, layout=layout)
    plotly.offline.plot(fig, filename='basic-line2')


def plotBarchartDistances(distId, distVect, names, title):
    import plotly
    traceList = list()
    for i, val in enumerate(distVect):
        traceList.append(plotly.graph_objs.Bar(x=distId[i], y=distVect[i], name=names[i]))
    layout = plotly.graph_objs.Layout(title=title, barmode='group')
    fig = plotly.graph_objs.Figure(data=traceList, layout=layout)
    plotly.offline.plot(fig, filename='basic-line2')


def compareGenomesFromPklsFolder(path):
    import os
    pathList = list()
    for fileName in os.listdir(path):
        pathList.append(path + '/' + fileName)
    compareGenomesFromPkls(pathList)
