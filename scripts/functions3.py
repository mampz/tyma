# Marco Muñoz Pérez - 77232669A
# Bioinfomática, Ingeniería de la Salud, Univeridad de Málaga
from tqdm import tqdm

from hitsOnBothSequences import get_line_number


def appendToFile(string, f):
    "Writes passed string to filePath and adds a new line."
    f.write(string + '\n')


def retMap(nuc):
    """Returns the mapping result for char nuc or False"""
    map_b = dict(A='0', C='1', G='2', T='3')
    return (map_b.get(nuc, False))


def kmerToInt(kmer):
    "Converts a k-mer to an integer"
    tmp = ''
    for nuc in kmer:
        tmp += map[nuc]
    return tmp


def intToKmer(intN, k):
    "Returns k-mer string for the corresponding base4 number"
    kmer = ''
    map = {'0': 'A',
           '1': 'C',
           '2': 'G',
           '3': 'T',
           }

    for letter in intN:
        kmer += map[str(letter)]
    kmer = (k - len(kmer)) * 'A' + kmer
    return kmer


def computeFreqs(header, tmpPath, destPath, k):
    import os
    "Computes frequencies from the temporal file and stores the result to an output file."
    fdest = open(destPath, 'a')
    appendToFile(header, fdest)
    print("Sorting temporal sequence file for " + header)
    os.system('sort "' + tmpPath + '" -o "' + tmpPath + '"')
    print('Saving to file...')
    f = open(tmpPath, 'r')
    kmerC = f.readline().replace('\n', '')
    kmerFreq = 1
    for i, kmer in enumerate(f):
        kmer = kmer.replace('\n', '')
        if kmerC == kmer:
            kmerFreq += 1
        elif kmer != ('', None, '\n'):
            appendToFile(intToKmer(kmerC, k) + ':' + str(kmerFreq), fdest)
            kmerC = kmer
            kmerFreq = 1
    open(tmpPath, 'w').close()


def manage(f, char):
    if retMap(char) != False:
        return (f, retMap(char))
    elif char == '>':
        return (f, char)
    else:
        c = char
        while char == c or char == '\n':
            char = f.read(1).upper()
        return manage(f, char)


def freqsFileToDf(filePath):
    import pandas as pd
    line_count = get_line_number(filePath)
    dfT = pd.DataFrame()
    df = pd.DataFrame()
    f = open(filePath, 'r')
    f = tqdm(f, total=get_line_number(filePath))
    for line in f:
        line = line.strip()
        if line.find('>') != -1:
            dfT = pd.concat([dfT, df])
            df = pd.DataFrame(index=[line])
        else:
            nuc = line[:line.find(':')]
            freq = int(line[line.find(':') + 1:])
            df.loc[[df.index[0]], nuc] = freq
    dfT = pd.concat([dfT, df])
    return (dfT)


def getKmerFreq(fileOrigin, tmpPath, freqDest, k):
    import os
    open(freqDest, 'w').close()
    file = open(fileOrigin, 'r')
    open(tmpPath, 'w').close()
    ftmp = open(tmpPath, 'a')
    lastPos = file.seek(0, 2)
    file.seek(0, 0)
    header = file.readline().replace('\n', '')
    while file.tell() <= lastPos - k:
        if header.find('-') == -1:  # Header indicates that reading is forwards.
            print('Progreso de separación de k-mers (k=' + str(k) + ')\t' + str(
                round((file.tell() / lastPos) * 100)) + '%\r', end='', flush=True)
            kmerSeq = ''
            for n in range(k):
                file.seek(file.tell(), 0)
                char = file.read(1).upper()
                if char != '>':
                    if lastPos - k <= file.tell():
                        break
                    file, char = manage(file, char)
                    if char != '>':
                        kmerSeq += char

                if char == '>':
                    ftmp.close()
                    if len(kmerSeq) == k:
                        appendToFile(kmerSeq, ftmp)
                    computeFreqs(header, tmpPath, freqDest, k)
                    ftmp = open(tmpPath, 'a')
                    header = '>' + file.readline().replace('\n', '')
                    break
            if len(kmerSeq) == k:
                appendToFile(kmerSeq, ftmp)
        else:  # '-' sign found in the header, reading must be done backwords.
            print('here1')
            startPoint = file.tell()  # Get the position of the pointer at the beggining of the sequence.
            while file.read(
                    1) != '>' and file.tell() != lastPos:  # While file pointer is not at new header or at end of file
                file.seek(file.tell(), 0)
                pass
            print('here2')
            endPoint = file.tell() - 1
            end = False
            while end == False:
                print('here3')
                kmerSeq = ''
                for n in range(k):
                    print('here4')
                    pos = file.seek(file.tell() - 2, 0)
                    if file.tell() == startPoint:
                        end = True
                        break
                    char = file.read(1).upper()
                    if char == '\n':
                        pos = file.seek(file.tell() - 2, 0)
                        print('before:' + str(file.tell()))
                        print(char)
                        char = file.read(1).upper()
                        print(file.tell())
                    charMap = retMap(char)
                    if charMap != False:
                        kmerSeq += charMap
                    elif char == 'N':
                        break
                if len(kmerSeq) == k:
                    print('here5')
                    appendToFile(kmerSeq, ftmp)
            file.seek(endPoint, 0)
            if file.read(1) == '>':
                file.seek(file.tell() - 1, 0)
                computeFreqs(header, tmpPath, freqDest, k)
                ftmp = open(tmpPath, 'a')
                header = file.readline().replace('\n', '')
    print('here')
    ftmp.close()
    computeFreqs(header, tmpPath, freqDest, k)
    file.close()
    os.remove(tmpPath)
    print('Done!')


def getIndexFromFile(filePath, df):
    import pandas as pd
    dfT = pd.DataFrame()
    f = open(filePath, 'r')
    for line in f:
        dfT = pd.concat([dfT, df[line.strip():line.strip()]])
    f.close()
    return dfT


def dfIndexToFile(filePath, df):
    f = open(filePath, 'w')
    for index in df.index:
        f.write(index + '\n')
    f.close()


def pDistanceBetweenRows(row1, row2, p):
    import math
    res = 0;
    for val1, val2 in zip(row1, row2):
        val = pow(abs(val1 - val2), p)
        res += val
    return pow(res, 1 / p)


def calculateDistances(df, p):
    matrix = list()
    for i in range(df.shape[0]):
        matrix.append(list())
        for j in range(df.shape[0]):
            matrix[i].append(pDistanceBetweenRows(df.iloc[i], df.iloc[j], p))
    return matrix


def getDistancesOrdered(matrix, index):
    import copy
    order = list()
    m = copy.deepcopy(matrix[index])
    r = list(range(len(m)))
    min = 0
    while r != []:
        for i in r:
            if (m[i] < m[min]):
                min = i
        order.append(min)
        r.remove(min)
        if r != []:
            min = r[0]
    return order


def modPlotDists(distV, values):
    values = [x * 10 for x in values]
    import plotly
    scales = ['<b>Window length: 2</b>', '<b>Window length: 3</b>', '<b>Window length: 5</b>']
    scale1 = list()
    # Define Titles and Labels for Each Scale
    for i, val in enumerate(distV):
        print(val)
        scale1.append(i)
    scale_labels = [scale1]
    # Add Scale Titles to the Plot
    traces = []
    for i in range(len(scales)):
        traces.append(plotly.graph_objs.Scatter(
            x=[1.5],  # Pad the title - a longer scale title would need a higher value
            y=[values[-1] + 0.25],
            text=scales[i],
            mode='text',
            hoverinfo='none',
            showlegend=False,
            xaxis='x' + str(i + 1),
            yaxis='y' + str(i + 1)
        ))

    # Create Scales
    ## Since we have 7 lables, the scale will range from 0-6
    shapes = []
    for i in range(len(scales)):
        shapes.append({'type': 'rect',
                       'x0': .02, 'x1': 1.02,
                       'y0': 0, 'y1': len(distV) - 1,
                       'xref': 'x' + str(i + 1), 'yref': 'y' + str(i + 1)})

    x_domains = [[0, .25], [.25, .5], [.5, .75], [.75, 1]]  # Split for 4 scales
    chart_width = 800

    # Define X-Axes
    xaxes = []
    for i in range(len(scales)):
        xaxes.append({'domain': x_domains[i], 'range': [0, 4],
                      'showgrid': False, 'showline': False,
                      'zeroline': False, 'showticklabels': False})

    # Define Y-Axes (and set scale labels)
    ## ticklen is used to create the segments of the scale,
    ## for more information see: https://plot.ly/python/reference/#layout-yaxis-ticklen
    yaxes = []
    for i in range(len(scales)):
        yaxes.append({'anchor': 'x' + str(i + 1), 'range': [-.5, values[-1] + 0.5],
                      'showgrid': False, 'showline': False, 'zeroline': False,
                      'ticks': 'inside', 'ticklen': chart_width / 20,
                      'ticktext': scale_labels[i], 'tickvals': values
                      })

    # Put all elements of the layout together
    layout = {'shapes': shapes,
              'xaxis1': xaxes[0],
              'yaxis1': yaxes[0],
              'autosize': False,
              'width': chart_width,
              'height': 600
              }

    ### ADD RATING DATA HERE ###

    fig = dict(data=traces, layout=layout)
    plotly.offline.plot(fig, filename='linear-gauge-layout')


def getHeadersFromFile(filePath):
    f = open(filePath, 'r')
    end = f.seek(0, 2)
    curr = -1;
    f.seek(0, 0)
    res = dict()
    while (f.tell() != end):
        if curr < round((f.tell() / end) * 100):
            curr += 1;
            print(str(curr) + '%', end='\r')
        lastLine = f.tell()
        l = f.readline()
        if l[0] == '>':
            res[lastLine] = l
            print(str(lastLine) + ':' + l)
    return res


def getSeqToFile(numPos, fileIn, fileOut):
    fi = open(fileIn, 'r')
    fo = open(fileOut, 'a')
    for i, pos in enumerate(numPos):
        print('Progress: ' + str(round((i / len(numPos)) * 100)) + '%', end='\r')
        fi.seek(pos, 0)
        line = fi.readline()
        fo.write(line)
        line = fi.readline()
        while (line[0] != '>' and line != ''):
            fo.write(line)
            line = fi.readline()
    fi.close()
    fo.close()


def addHeaderToFile(filename, line):
    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line + '\n' + content)


def getRegion(filePath, startPos, endPos):
    sequence = ''
    with open(filePath, 'r') as file:
        if file.read(1) == '>':
            file.readline()
        else:
            file.seek(0, 0)
        n = 0
        if (startPos < endPos):
            while (n < startPos):
                if (file.read(1) != '\n'):
                    n += 1

            while (n < endPos):
                char = file.read(1)
                if char != '\n':
                    sequence = sequence + char
                    n += 1
        else:
            while (n < endPos):
                if (file.read(1) != '\n'):
                    n += 1

            while (n < startPos):
                char = file.read(1)
                if char != '\n':
                    sequence = sequence + char
                    n += 1
            sequence = sequence[::-1]

    return sequence
