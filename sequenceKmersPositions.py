# Author: Marco Munoz Perez
# Email: marcomunozperez@uma.es

from Bio import SeqIO
from tqdm import tqdm

from hitsOnBothSequences import get_line_number


def fasta_to_str(file_path):
    sequence = str(list(SeqIO.parse(file_path, "fasta"))[0].seq)
    return sequence


# In[2]:
def str_to_kmer_positions(sequence, k, out_file_path):
    """
    This function will generate a file consisting of lines with a
    kmer of length k, passed as arg, and its position in the sequence
    passed as arg.
    :param sequence: Input sequence
    :param k: kmer size window length
    :param out_file_path: Destination file path for kmer-position
    :return: Returns nothing. Creates a kmer-position file
    """
    with open(out_file_path, 'w') as output_file:
        for i in range(len(sequence) - k):
            output_file.write(sequence[i:i + k] + ',' + str(i) + '\n')


# In[]:
def get_kmer_hits_file(kmer_filepath1, kmer_filepath2, hits_filepath):
    """
    Given two file paths of kmer-position lines, it will generate a hits file
    consisting of kmers that appear on both files, followed by both positions.
    :param kmer_filepath1: kmer - pos file location for sequence 1.
    :param kmer_filepath2: kmer - pos file location for sequence 2.
    :param hits_filepath: Location for the hits file.
    """
    # print('Hits file generator initialized...')

    file1 = open(kmer_filepath1, 'r')
    file2 = open(kmer_filepath2, 'r')
    hits_file = open(hits_filepath, 'w')

    # This variable is initialized to any string
    current_kmer1 = 'INIT'
    line2_current_kmer_file_pointer = file2.tell()
    tmp_pos2 = file2.tell()
    line2 = file2.readline().replace(',', ' ')
    for line1 in tqdm(file1, total=get_line_number(kmer_filepath1)):
        line1 = line1.replace(',', ' ')
        if current_kmer1 == line1.split()[0]:
            file2.seek(line2_current_kmer_file_pointer, 0)
            line2 = file2.readline().replace(',', ' ')
        elif line1.split()[0] == line2.split()[0]:
            current_kmer1 = line1.split()[0]
            hits_file.write('\n')
            hits_file.write(current_kmer1 + ' ')
            line2_current_kmer_file_pointer = tmp_pos2

        while line2.split()[0] == line1.split()[0]:
            tmp_pos2 = file2.tell()
            hits_file.write(line1.split()[1].strip('\n') + ',' + line2.split()[1].strip('\n') + ';')
            line2 = file2.readline().replace(',', ' ')
            if line2 == '':
                break
        if line2 == '':
            break

        while line1.split()[0] > line2.split()[0]:
            tmp_pos2 = file2.tell()
            line2 = file2.readline().replace(',', ' ')
        # hits_file.flush() #Only for debugging purposes.

    file1.close()
    file2.close()
    hits_file.close()
    # print('Hits file generator ended')

# In[3]:

# file_name_in1 = 'TestSequences/Myco01_k10.txt'
# file_name_out = 'TestSequences/Myco01_k10.txt'

# str_to_kmer_positions(fasta_to_str(file_name_in), 10, file_name_out)

# In[4]:

# file_name_in2 = 'TestSequences/Myco02_k10.txt'
# file_name_out = 'TestSequences/Myco02_k10.txt'

# str_to_kmer_positions(fasta_to_str(file_name_in), 10, file_name_out)

# In[]:
#file1 = '/Users/marcomunozperez/PycharmProjects/ISA/TyMA/KmerPositions/sequence1Sorted_k=10_kmerPositions.txt'
#file2 = '/Users/marcomunozperez/PycharmProjects/ISA/TyMA/KmerPositions/sequence2Sorted_k=10_kmerPositions.txt'
# fileOut = 'TestSequences/testOut.txt'
# get_kmer_hits_file(file1, file2, fileOut)

#fout = 'TestSequences/otest.txt'

#get_kmer_hits_file(file1, file2, fout)
